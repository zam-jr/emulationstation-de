# Modern for EmulationStation Desktop Edition (modern-es-de)

The following options are included:

2 variants:

- Textlist with videos
- Textlist without videos

2 color schemes:

- Dark mode
- Light mode

4 aspect ratios:

- 16:9
- 16:10
- 4:3
- 21:9

3 transitions:

- Instant
- Instant and slide
- Instant and fade

# Credits

The theme is based on [es-theme-switch](https://github.com/lilbud/es-theme-switch) by lilbud.
